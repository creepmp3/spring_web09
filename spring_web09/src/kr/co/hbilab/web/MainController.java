package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 2015. 6. 3.
 * @author KDY
 */

@Controller
public class MainController {
    
    @RequestMapping(value="/main")
    public String main(Model model, HttpServletRequest req){
        HttpSession session = req.getSession();
        String id = (String)session.getAttribute("id");
        
        model.addAttribute("id", id);
        session.setAttribute("url", req.getRequestURI());
        return "/main";
    }
}
