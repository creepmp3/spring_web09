<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript">
function validate(){
	if($("#id").val() == ""){
		alert("ID를 입력하세요");
		$("#id").focus();
		return false;
	}
	if($("#pw").val() == ""){
        alert("PW를 입력하세요");
        $("#pw").focus();
        return false;
    }
	if($("#pw2").val() == ""){
        alert("Confirm PW를 입력하세요");
        $("#pw2").focus();
        return false;
    }
	if($("#pw").val() != $("#pw2").val()){
        alert("암호가 일치하지 않습니다");
        $("#pw").val("");
        $("#pw2").val("");
        $("#pw").focus();
        return false;
    }

    if($("#email").val() == ""){
        alert("EMAIL를 입력하세요");
        $("#email").focus();
        return false;
    }
    return true;
}
</script>
<title>Insert title here</title>
<style type="text/css">
table {
}
table tr th, table tr td {
     border:1px solid black;
     border-collapse:collapse;
}
table tr th {
    text-align:right;
    background-color: #dedede;
}
table tr:last-child td {
    text-align:right;
    border:0px solid black;
}
</style>
</head>
<body>

    <h3>회원가입 - Step2</h3>
    <form action="step3.do" id="frm" method="post" onsubmit="return validate()">
        <table>
            <tr>
                <th>ID</th>
                <td><input type="text" name="id" id="id" /></td>
            </tr>
            <tr>
                <th>PW</th>
                <td><input type="text" name="pw" id="pw" /></td>
            </tr>
            <tr>
                <th>Confirm PW</th>
                <td><input type="text" name="pw2" id="pw2" /></td>
            </tr>
            <tr>
                <th>EMAIL</th>
                <td><input type="text" name="email" id="email" /></td>
            </tr>
            <tr>
                <td colspan="2">
                   <input type="submit" value="회원가입" />
                   <input type="button" value="되돌아가기" onclick="javascript:history.back();"/>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>