<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/view/common/header.jsp"></jsp:include>
<style type="text/css">
table {
     border-collapse:collapse;
     text-align:center;
}
table tr th, table tr td {
     border:1px solid black;
     border-collapse:collapse;
}
table tr th {
    background-color: #dedede;
}
</style>
    
    <table>
        <tr>
            <th>ID</th>
            <th>PW</th>
            <th>EMAIL</th>
        </tr>
	    <c:forEach items="${list }" var="m">
	    <tr>
	        <td><c:out value="${m.id}"></c:out></td>
	        <td><c:out value="${m.pw}"></c:out></td>
	        <td><c:out value="${m.email}"></c:out></td>
	    </tr>
	    </c:forEach>
	</table>
</body>
</html>