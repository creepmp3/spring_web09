package kr.co.hbilab.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * 2015. 5. 29.
 * @author KDY
 * 
 */

@Controller
public class ViewMenuController {

    @RequestMapping(value="/viewData.do")
    public ModelAndView show(){
        ModelAndView model = new ModelAndView("view", "data", "View Data");
        return model;
    }
    
}
