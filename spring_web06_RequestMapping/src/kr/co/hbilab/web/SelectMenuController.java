package kr.co.hbilab.web;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * 2015. 5. 29.
 * @author KDY
 * @comment Controller
 */

@Controller
public class SelectMenuController{
    
    @RequestMapping(value="/selectMenu.do")
    public ModelAndView test(){
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("menu", "한식");
        map.put("menu", "중식");
        map.put("menu", "일식");
        map.put("menu", "양식");
        list.add(map);
        ModelAndView model = new ModelAndView("menu", "text", list);
        return model;
    }
    
    @RequestMapping(value="/data.do")
    public ModelAndView data(){
        ModelAndView model = new ModelAndView("list", "data", "전달데이터");
        return model;
    }
}
