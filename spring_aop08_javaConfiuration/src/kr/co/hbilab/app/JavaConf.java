package kr.co.hbilab.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 2015. 5. 28.
 * @author user
 * 
 * @Configuration : 이 클래스는 xml대신 설정 정보를 가지고 있다
 */

@Configuration
@EnableAspectJAutoProxy  // app.xml 의 <aop:aspectj-autoproxy />
public class JavaConf {
    
    // CustomerServiceImple클래스를 Bean으로 설정
    // <bean id="cs" class="kr.co.hbilab.app.CustomerServiceImple">
    @Bean(name="cs")
    public CustomerServiceImple customerServiceImple(){
        CustomerServiceImple csi = new CustomerServiceImple();
        csi.setName("나와라 점심");
        csi.setEmail("오늘의 메뉴?");
        return csi;
    }
    
    @Bean
    public CheckTime checkTime(){
        return new CheckTime();
    }
}
