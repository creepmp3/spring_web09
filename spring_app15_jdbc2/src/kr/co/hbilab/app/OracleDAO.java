package kr.co.hbilab.app;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

//20150526
public class OracleDAO implements Dao{
    
    private JdbcTemplate jdbcTemplate;
    StringBuffer sql = new StringBuffer();
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<EmpDTO> selectAll() {
        
        RowMapper<EmpDTO> rm = new RowMapper<EmpDTO>(){
            @Override
            public EmpDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                int empno = rs.getInt("empno");
                String ename = rs.getString("ename");
                String job = rs.getString("job");
                int mgr = rs.getInt("mgr");
                String hiredate = rs.getString("hiredate");
                int sal = rs.getInt("sal");
                int comm = rs.getInt("comm");
                int deptno = rs.getInt("deptno");
                
                EmpDTO dto = new EmpDTO(empno, ename, job, mgr, hiredate, sal, comm, deptno);
                
                return dto;
            }
        };
        
        sql.setLength(0);
        sql.append("\n SELECT EMPNO                                    ");
        sql.append("\n      , ENAME                                    ");
        sql.append("\n      , RPAD(JOB, 9, ' ') JOB                    ");
        sql.append("\n      , MGR                                      ");
        sql.append("\n      , TO_CHAR(HIREDATE, 'YYYY-MM-DD') HIREDATE ");
        sql.append("\n      , SAL                                      ");
        sql.append("\n      , COMM                                     ");
        sql.append("\n      , DEPTNO                                   ");
        sql.append("\n  FROM EMP                                       ");
        
        List<EmpDTO> list = jdbcTemplate.query(sql.toString(), rm);
        
        return list;
    }

    @Override
    public EmpDTO selectOne(int no) {
        RowMapper<EmpDTO> rm = new RowMapper<EmpDTO>(){
            @Override
            public EmpDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                int empno = rs.getInt("empno");
                String ename = rs.getString("ename");
                String job = rs.getString("job");
                int mgr = rs.getInt("mgr");
                String hiredate = rs.getString("hiredate");
                int sal = rs.getInt("sal");
                int comm = rs.getInt("comm");
                int deptno = rs.getInt("deptno");
                
                EmpDTO dto = new EmpDTO(empno, ename, job, mgr, hiredate, sal, comm, deptno);
                return dto;
            }
        };
        
        sql.setLength(0);
        sql.append("\n SELECT * FROM EMP ");
        sql.append("\n  WHERE EMPNO = ?  ");
        EmpDTO dto = jdbcTemplate.queryForObject(sql.toString(), rm, no);
        return dto;
    }

    @Override
    public void insertOne(EmpDTO dto) {
        sql.setLength(0);
        sql.append("\n INSERT INTO EMP(EMPNO    ");
        sql.append("\n               , ENAME    ");
        sql.append("\n               , JOB      ");
        sql.append("\n               , MGR      ");
        sql.append("\n               , HIREDATE ");
        sql.append("\n               , SAL      ");
        sql.append("\n               , COMM     ");
        sql.append("\n               , DEPTNO   ");   
        sql.append("\n                )VALUES(? "); // EMPNO
        sql.append("\n                      , ? "); // ENAME
        sql.append("\n                      , ? "); // JOB
        sql.append("\n                      , ? "); // MGR
        sql.append("\n                      , SYSDATE "); // HIREDATE
        sql.append("\n                      , ? "); // SAL
        sql.append("\n                      , ? "); // COMM
        sql.append("\n                      , ? "); // DEPTNO
        sql.append("\n                       )  ");
        int result = jdbcTemplate.update(sql.toString(), dto.getEmpno(), dto.getEname(), dto.getJob(), dto.getMgr(), dto.getSal(), dto.getComm(), dto.getDeptno());
        if(result>0){
            System.out.println("등록완료");
        }else{
            System.out.println("등록실패");
        }
    }

    @Override
    public void updateOne(EmpDTO dto) {
        sql.setLength(0);
        sql.append("\n UPDATE EMP SET    ");
        sql.append("\n        ENAME = ?  ");
        sql.append("\n      , JOB = ?    ");
        sql.append("\n      , MGR = ?    ");
        sql.append("\n      , SAL = ?    ");
        sql.append("\n      , COMM = ?   ");
        sql.append("\n      , DEPTNO = ? ");
        sql.append("\n  WHERE EMPNO = ?  ");
        
        int result = jdbcTemplate.update(sql.toString(), dto.getEname(), dto.getJob(), dto.getMgr(), dto.getSal(), dto.getComm(), dto.getDeptno(), dto.getEmpno());
        if(result>0){
            System.out.println("수정완료");
        }else{
            System.out.println("수정실패");
        }
    }

    @Override
    public void deleteOne(int no) {
        sql.setLength(0);
        sql.append("\n DELETE FROM EMP  ");
        sql.append("\n  WHERE EMPNO = ? ");
        int result = jdbcTemplate.update(sql.toString(), no);
        if(result>0){
            System.out.println("삭제완료");
        }else{
            System.out.println("삭제실패");
        }
    }

    
}
