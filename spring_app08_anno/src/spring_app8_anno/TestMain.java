package spring_app8_anno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain{
    public static void main(String[] args) {
        /*
        GreetingImpl gi = new GreetingImpl();
        gi.printMsg();
        */
        ApplicationContext context = new ClassPathXmlApplicationContext("app.xml");
        Greeting gi = context.getBean("gi", Greeting.class);
        gi.printMsg();
    }
}
