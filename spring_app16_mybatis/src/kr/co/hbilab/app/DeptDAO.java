package kr.co.hbilab.app;

import java.util.List;

//20150527
public class DeptDAO implements Dao{

    ConnectionManager cm;

    public void setCm(ConnectionManager cm) {
        this.cm = cm;
    }

    @Override
    public List<DeptDTO> selectAll() {
        /*
        SqlSessionFactory factory = cm.getFactory();
        SqlSession ss = factory.openSession(true);
        List<DeptDTO> list = ss.selectList("selectAll");
        return list;
        */
        return cm.getFactory().openSession(true).selectList("selectAll");
        
    }

    @Override
    public DeptDTO selectOne(int no) {
        return cm.getFactory().openSession(true).selectOne("selectOne", no);
    }
    
}
