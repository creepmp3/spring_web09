package kr.co.hbilab.app;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TestMain {
    public static void main(String[] args) {
        ApplicationContext ctx = new GenericXmlApplicationContext("app.xml");
        
        
        Dao dao = ctx.getBean("dao", Dao.class);
        List<DeptDTO> list = dao.selectAll();
        
        System.out.println("부서번호\t\t부서명\t\t부서위치");
        for(DeptDTO dto : list){
            System.out.print(dto.getDeptno()+"\t\t");
            System.out.print(dto.getDname()+"\t\t");
            System.out.print(dto.getLoc()+"\n");
        }
        
        
        /*
        int no = 70;
        Dao dao = ctx.getBean("dao", Dao.class);
        DeptDTO dto = dao.selectOne(no);
        
        System.out.println("부서번호\t\t부서명\t\t부서위치");
        System.out.print(dto.getDeptno()+"\t\t");
        System.out.print(dto.getDname()+"\t\t");
        System.out.print(dto.getLoc()+"\n");
        */
        
        /*
        Dao dao = ctx.getBean("dao", Dao.class);
        DeptDTO dto = new DeptDTO();
        dto.setDeptno(70);
        dto.setDname("부서명70");
        dto.setLoc("부서위치70");
        dao.indertOne(dto);
        */
        
        /*
        Dao dao = ctx.getBean("dao", Dao.class);
        DeptDTO dto = new DeptDTO();
        dto.setDeptno(70);
        dto.setDname("부서명77");
        dto.setLoc("부서위치77");
        dao.updateOne(dto);
        */
        
        /*
        Dao dao = ctx.getBean("dao", Dao.class);
        dao.deleteOne(60);
        */
    }
}
