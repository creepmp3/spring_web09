<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table tr th, table tr td {
     border:1px solid black;
     border-collapse:collapse;
}
table tr th {text-align:right;background-color: #dedede;}
table tr:last-child td {
    text-align:right;
    border:0px solid black;
}
</style>
</head>
<body>
    <h3>Login</h3>
    <form action="loginOk" method="post">
	    <table>
	        <tr>
	        	<th>ID</th>
	        	<td><input type="text" name="id" id="id" /></td>
	        </tr>
	        <tr>
	        	<th>PW</th>
	        	<td><input type="text" name="pw" id="pw" /></td>
	        </tr>
	        <tr>
	            <td colspan="2">
	                <input type="submit" value="로그인" />
	            </td>
	        </tr>
	    </table>
	</form>
</body>
</html>