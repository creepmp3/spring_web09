package kr.co.hbilab.app;

public class Police implements Character{
    /*
    Gun g;
    
    public Police(){
        
    }
    
     public Police(Weapon w){
        super();
        this.w = w;
    }
    
    public Police(Gun g){
        this.g = g;
    }
    
    
    public void setG(Gun g){
        this.g = g;
    }
    public Police(Weapon w, int hp){
        super();
        this.w = w;
        this.hp = hp;
        System.out.println("hp : " + hp);
    }
    
    */

    
    Weapon w;
    int hp;
    
    public void setW(Weapon w){
        this.w = w;
    }
    
    public void setHp(int hp){
        this.hp = hp;
        System.out.println("hp : " + hp);
    }
    
    @Override
    public void walk() {
        System.out.println("뚜벅뚜벅...");
    }

    @Override
    public void eat(String type) {
        System.out.println(type+"와 도넛을 먹어요");
    }

    @Override
    public void attack(Object obj) {
        
        System.out.println("\n"+obj + " 공격..");
        if(w!=null){
            w.use();
        }
    }

    @Override
    public void get(Object obj) {
        System.out.println(obj + " 획득!");
    }
    
}
