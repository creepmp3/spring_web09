package kr.co.hbilab.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OracleDAO implements Dao{

    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    StringBuffer sql = new StringBuffer();
    
    String driver = "oracle.jdbc.driver.OracleDriver";
    String url = "jdbc:oracle:thin:@192.168.0.80:1521:orcl";
    String username = "scott";
    String password = "tiger";
    
    @Override
    public int selectCount() {
        int cnt = 0;
        try{
            sql.setLength(0);
            sql.append("\n SELECT COUNT(*) cnt FROM DEPT ");
            pstmt = conn.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();
            if(rs.next()) cnt = rs.getInt("cnt");
        }catch(SQLException e){
            e.printStackTrace();
        }
        return cnt;
    }

    public void close() throws Exception {
        if(rs != null) rs.close();
        if(pstmt != null) pstmt.close();
        if(conn != null) conn.close();
        System.out.println("DB접속 종료");
    }

    public void init() throws Exception {
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
            System.out.println("DB접속 성공");
        }catch(ClassNotFoundException e){
            System.out.println("드라이버 로딩 실패");
        }catch(SQLException e){
            System.out.println("DB 연결 실패");
        }
    }
    
}
