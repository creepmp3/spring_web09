<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
.error {
    color:red;
}
table, table tr th, table tr td{
    border:1px solid black;
    border-collapse: collapse;
}
table tr th {
}
</style>
</head>
<body>
    <jsp:include page="/WEB-INF/view/header.jsp"></jsp:include>
    <form:form action="registerOk" method="get" cssClass="form" commandName="dto">
        <table>
        	<tr>
        		<th><form:label path="id">ID</form:label></th>
        		<td><form:input path="id"/></td>
        		<td><form:errors path="id" cssClass="error"></form:errors></td>
        	</tr>
        	<tr>
        		<th><form:label path="pw">PW</form:label></th>
        		<td><form:input path="pw"/></td>
                <td><form:errors path="pw" cssClass="error"></form:errors></td>
        	</tr>
        	<tr>
        		<th><form:label path="confirm">Confirm</form:label></th>
        		<td><form:input path="confirm"/></td>
                <td><form:errors path="confirm" cssClass="error"></form:errors></td>
        	</tr>
        	<tr>
        		<th><form:label path="email">Email</form:label></th>
        		<td><form:input path="email"/></td>
                <td><form:errors path="email" cssClass="error"></form:errors></td>
        	</tr>
        	<tr>
        		<td colspan="3"><input type="submit" value="등록" /></td>
        	</tr>
        </table>
    </form:form>
</body>
</html>