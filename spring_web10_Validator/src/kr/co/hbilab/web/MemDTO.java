/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

public class MemDTO {
    private String id;
    private String pw;
    private String confirm;
    private String email;
    
    public MemDTO() {
    }

    public MemDTO(String id, String pw, String confirm, String email) {
        super();
        this.id = id;
        this.pw = pw;
        this.confirm = confirm;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }
    

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
