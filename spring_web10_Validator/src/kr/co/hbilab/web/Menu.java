/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

public class Menu {
    private int menuno;
    private String name;
    private int price;
    
    
    
    public Menu() {
    }
    
    
    
    public Menu(int menuno, String name, int price) {
        super();
        this.menuno = menuno;
        this.name = name;
        this.price = price;
    }



    public int getMenuno() {
        return menuno;
    }
    public void setMenuno(int menuno) {
        this.menuno = menuno;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    
    
}
