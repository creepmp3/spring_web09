/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormController {
    
    @RequestMapping("/form")
    public String form(Model model){
        MyForm myForm = new MyForm();
        model.addAttribute("myForm", myForm);
        List<Menu> menuList = loadMenu();
        model.addAttribute("menuList", menuList);
        return "form";
    }
    
    public List<Menu> loadMenu(){
        List<Menu> list = new ArrayList<Menu>();
        list.add(new Menu(1, "짜장", 5000));
        list.add(new Menu(2, "계절밥상", 14900));
        list.add(new Menu(3, "일산비빔국수", 6000));
        list.add(new Menu(4, "순대국", 7000));
        return list;
    }
}
