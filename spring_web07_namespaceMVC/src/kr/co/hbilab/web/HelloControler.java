package kr.co.hbilab.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping;

/**
 * 2015. 6. 1.
 * @author KDY
 * 
 * 
 * POJO (Plane Old Java Object) 객체 :
 * 
 * Controller : 일을하는 대상 Handler , Action : struts2
 *   1. Controller 인터페이스 구현 객체를 오버라이드해서 사용 (옛날방식)
 *   2. @Controller 어노테이션으로 사용
 * 
 */

@Controller
public class HelloControler {
    @SuppressWarnings("deprecation")
    DefaultAnnotationHandlerMapping d;
    
    @RequestMapping(value="/h.do", method=RequestMethod.GET)
    public String hi(){
        return "hello";
    }
    
    @RequestMapping("/hi.do")
    public ModelAndView hello(){
        return new ModelAndView("hello", "msg", "Hello ~ 드디어 6월");
    }
    
}
