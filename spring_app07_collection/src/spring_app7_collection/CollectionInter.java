package spring_app7_collection;

public interface CollectionInter{
    public void printList();
    public void printMap();
}
